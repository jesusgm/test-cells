import {LitElement, html, css} from 'lit-element';

class QuestionFive extends LitElement {

    static get styles() {
        return css`
        #div1 {
            width: 100%;
            height: 200px;
            padding: 10px;
            border: 1px solid #aaaaaa;
        }
        
        `;
    }

    static get properties() {
        return {

        }
    }

    constructor() {
        super();
    }

    _dragstart(ev){
        ev.dataTransfer.setData("text", ev.target.id);
    }

    _drop(ev){
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.appendChild(this.shadowRoot.getElementById(data));
    }
    _allowDrop(ev){
        ev.preventDefault();
    }

    render() {
        return html`
        <div id="div1" @drop="${this._drop}" @dragover="${this._allowDrop}"></div>
        <img id="drag1" src="https://www.animalshealth.es/fileuploads/news/wwfmesa-de-trabajo-11.jpg" draggable="true" @dragstart="${this._dragstart}" width="336" height="69">

        `;
    }

}

customElements.define('question-five', QuestionFive);