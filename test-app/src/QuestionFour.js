import {LitElement, html, css} from 'lit-element';

class QuestionFour extends LitElement{

    static get styles(){
        return css`
        :host{
            color: var(--question-four-color,blue);
        }
        
        `;
    }

    static get properties(){
        return{
            propDefault:{type:String,
                hasChanged(newValue, value){
                    console.log(value);
                    console.log(newValue);
                }},
        };
    }

    constructor(){
        super();
        this.propDefault = 'Some text';
        this.newArray = [1,2,3];
    }

    _changeValue(){
        let valInput = this.shadowRoot.querySelector('input');
        this.propDefault = valInput.value;
        this.requestUpdate();
    }

    render(){
        return html`
        <p>${this.propDefault}</p>
        <input type="text" >
        <button @click=${this._changeValue}>Change</button>
        <ul>
        ${this.newArray.map(data=>html`
            <li>${data}</li>
        `)}
        </ul>

        `;
    }
}

customElements.define('question-four', QuestionFour);